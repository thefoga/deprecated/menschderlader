# !/usr/bin/python3
# coding: utf-8


""" Example of running this module """

import os


def main():
    driver_path = os.path.join(
        os.getenv("HOME"), "Coding", "misc", "chromedriver"
    )
    output_folder = os.path.join(
        os.getenv("HOME"), "websites"
    )
    url = "https://bilanciapolitica.bitbucket.io"


if __name__ == '__main__':
    main()
