# !/usr/bin/python3
# coding: utf_8


""" Setups library and install dependencies """

from setuptools import setup, find_packages

DESCRIPTION = \
    "Menschderlader\n\n\ A bot that mimics a human while downloading files.\n\ \n\ What it is?\n\n\ Ever wonder how " \
    "to download entire Wordpress sites? No, you can't with neither wget nor httrack! So what tool can I use? None " \
    "that I know. That's how menschderlader was born! It simulates a menschlicher (human) that herunderlader (" \
    "downloads) files from the Internet. It can't use any of the low-level implementation of python downloaders, " \
    "thus it leverages selenium power to download directly pages.\n\ \n\ Install\n\n\ - $ pip3 install .  # using pip" \
    "\n\n\License: Apache License Version 2.0, January 2004"

setup(
    name="menschderlader",
    version="0.1",
    author="sirfoga",
    author_email="sirfoga@protonmail.com",
    description="A bot that mimics a human while downloading files",
    long_description=DESCRIPTION,
    license="Apache License, Version 2.0",
    keywords="library bot downloader selenium",
    url="https://github.com/sirfoga/menschderlader",
    packages=find_packages(exclude=["tests"]),
    install_requires=["httplib2", "beautifulsoup4", 'selenium', 'requests'],
    # also pyhal --> https://github.com/sirfoga/pyhal
    test_suite="tests",
    entry_points={
        "console_scripts": ["menschderlader = menschderlader.cli:cli"]
    }
)
