<div align="center">
<h1>Menschderlader</h1>
<em>A bot that mimics a human while downloading files</em></br></br>
</div>

<div align="center">
<a href="https://www.python.org/download/releases/3.6.0/"><img alt="Python version" src="https://img.shields.io/badge/Python-3.6-blue.svg"></a> <a href="https://opensource.org/licenses/Apache-2.0"><img alt="Open Source Love" src="https://badges.frapsoft.com/os/v1/open-source.svg?v=103"></a> <a href="https://github.com/sirfoga/menschderlader/issues"><img alt="Contributions welcome" src="https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat"></a> <a href="https://www.apache.org/licenses/LICENSE-2.0"><img alt="License" src="https://img.shields.io/badge/license-Apache%202.0-blue.svg"></a>
</div>


## What it is?
Ever wonder how to download **entire** Wordpress sites? No, you can't with neither [wget](https://superuser.com/questions/721854/mirroring-a-wordpress-website-with-wget) nor [httrack](https://forum.httrack.com/readmsg/33400/23772/index.html)!
So what tool can I use? None that I know. That's how menschderlader was born! It simulates a *menschlicher* (human) that *herunderlader* (downloads) files from the Internet.
It can't use any of the low-level implementation of python downloaders, thus it leverages [selenium](https://www.seleniumhq.org/) power to download directly pages.


## Install
```shell
$ pip3 install .  # using pip
```
*This will take care of all dependencies*


## Usage
```shell
$ python3 run.py
```

## Contributing
[Fork](https://github.com/sirfoga/menschderlader/fork) | Patch | Push | [Pull request](https://github.com/sirfoga/menschderlader/pulls)


## Feedback
Suggestions and improvements [welcome](https://github.com/sirfoga/menschderlader/issues)!


## Authors
| [![sirfoga](https://avatars0.githubusercontent.com/u/14162628?s=128&v=4)](https://github.com/sirfoga "Follow @sirfoga on Github") |
|---|
| [Stefano Fogarollo](https://sirfoga.github.io) |


## License
[Unlicense](https://unlicense.org/)
