# !/usr/bin/python3
# coding: utf-8


""" Bot downloading pages """

import os
import random
import shutil

import requests
from hal.internet.web import CHROME_USER_AGENT

from menschderlader.models.data import SetQueue
from menschderlader.models.logs import Logger
from menschderlader.models.webpage import WebpageParser
from menschderlader.utils.files import is_folder
from menschderlader.utils.web import get_hostname, is_url_from_root, get_relative_path


class Browser(Logger):
    """ Downloads web pages like a human """

    def __init__(self, url, output_folder, verbose=False):
        """
        :param url: str
            Url to download
        :param output_folder: str
            Path to output folder
        :param chrome_driver_path: str
            Path to Chrome driver
        :param verbose: bool
            True iff you want verbose output
        """

        Logger.__init__(self, verbose)

        self.start_url = url
        self.root_hostname = get_hostname(self.start_url)

        self.current_url, self.current_source = None, None

        self.download_queue = SetQueue()
        self.download_queue.put(url)
        self.downloaded_urls = []  # list of already downloaded urls

        self.output = os.path.join(output_folder, self.root_hostname)
        self.tmp_output = os.path.join(output_folder, ".tmp")

        self._create_output_folder()

    def download(self, recurse):
        """
        :param recurse: bool
            True iff you want to recursively download sub-pages of same host
        :return: void
            Downloads page to folder
        """

        self.log("urls in queue:", self.download_queue.qsize())
        if self.download_queue.empty():  # no more urls to get
            self._clean()  # clean tmp files
            return

        self._save(self.download_queue.get())

        if recurse:
            links = self._get_links()  # find links
            for link in links:  # add urls to queue
                self._add_to_queue(link)
        self.download(recurse)

    def _save(self, url):
        """
        :param url: str
            Url to download
        :return: void
            Downloads current page to output folder
        """

        output_path = self._get_output_path(url)
        if output_path:
            output_folder = os.path.dirname(output_path)
            if not os.path.exists(output_folder):
                os.makedirs(output_folder)  # create necessary folders

            self.download_to_file(url, output_path)  # todo edit hrefs before saving page
            self.downloaded_urls.append(url)  # add to list of downloaded urls

    def download_to_file(self, url, local_file, headers=None, cookies=None,
                         chunk_size=1024):
        """
        :param url: str
            PDF url to download
        :param local_file: str
            Save url as this path
        :param headers: {}
            Headers to fetch url
        :param cookies: {}
            Cookies to fetch url
        :param chunk_size: int
            Download file in this specific chunk size
        :return: void
            Download link to local file
        """

        self.current_url = url

        if headers is None:
            headers = {"User-Agent": random.choice(CHROME_USER_AGENT)}
        if not cookies:
            cookies = {}

        req = requests.get(url, headers=headers, cookies=cookies, stream=True)
        try:
            self.current_source = req.content.decode()
            self._fix_links_in_page()
            req._content = bytes(self.current_source, "utf-8")
        except:  # maybe it's not even a page
            pass

        with open(local_file, "wb") as local_download:
            for chunk in req.iter_content(chunk_size):
                if chunk:
                    local_download.write(chunk)

    def _fix_links_in_page(self):
        """
        :return: void
            Fixes href, src attributes of links in current page
        """

        page = WebpageParser(self.current_url, self.current_source)
        page.edit_links(self.link_fixer, just_from_root=True)

    def _get_output_path(self, url):
        """
        :param url: str
            Url to get output of
        :return: str
            Path to output file
        """

        # todo find way of saving external urls?
        if is_url_from_root(url, self.root_hostname):  # relative path
            output_file = self.output + get_relative_path(url)
            if is_folder(output_file):  # it's like a folder
                output_file = os.path.join(output_file, "index.html")

            return output_file

    def _get_links(self):
        """
        :return: [] of str
            List of links in current page with url
        """

        url = self.current_url
        if url is None or get_hostname(url) != self.root_hostname:
            return []  # download no external urls

        webpage = WebpageParser(url, self.current_source)
        return webpage.get_links(just_from_root=True, full_path=True)

    def _add_to_queue(self, url):
        """
        :param url: str
            Url to add to list of future downloads
        :return: bool
            True iff url has been appended
        """

        if url in self.downloaded_urls or url in self.download_queue:
            return False

        self.download_queue.put(url)
        return True

    def _create_output_folder(self):
        """
        :return: void
            Prepares output folder
        """

        if not os.path.exists(self.output):
            os.makedirs(self.output)

        if not os.path.exists(self.tmp_output):
            os.makedirs(self.tmp_output)

    def _clean(self):
        """
        :return: void
            Removes tmp files and folders
        """

        if os.path.exists(self.tmp_output):
            shutil.rmtree(self.tmp_output)

    def link_fixer(self, link):
        """
        :param link: BeautifulSoup tag
            Link
        :return: void
            Edit link attr
        """

        for attr in link.attrs:
            if is_url_from_root(str(link[attr]), self.current_url):
                link[attr] = get_relative_path(link[attr])
