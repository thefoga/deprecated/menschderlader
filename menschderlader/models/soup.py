# !/usr/bin/python3
# coding: utf-8


""" Models to deal with parsing web-pages """

import re

from bs4 import BeautifulSoup


class LinksSoup(BeautifulSoup):
    """ Extends standard BeautifulSoup with links tools """

    REGEX_STR = re.compile("^[a-zA-Z]+$")
    REGEX_ALSO_SYM_AND_NUM = re.compile(".")

    def get_attrs(self, attr_name):
        """
        :param attr_name: str
            Name of attribute to look for
        :return: generator of tuple (tag, str)
            Finds all tags with such attribute, and value of attribute
        """

        raw = self.find_all(self.REGEX_STR, {attr_name: self.REGEX_ALSO_SYM_AND_NUM})
        for item in raw:
            yield (item, item[attr_name])

    def get_links(self):
        """
        :return: [] of str
            List of all links
        """

        return list(self.get_attrs("href")) + list(self.get_attrs("src")) + list(self.get_attrs("action"))

    def insert_after(self, successor):
        pass

    def insert_before(self, successor):
        pass
