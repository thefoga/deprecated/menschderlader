# !/usr/bin/python3
# coding: utf-8


""" Models to deal with downloading web-pages """

import urllib.parse
import urllib.request

from hal.internet.web import is_url

from menschderlader.models.soup import LinksSoup
from menschderlader.utils.web import is_relative_url, is_url_from_root


class WebpageParser:
    """ Navigates through a webpage finding references and links """

    def __init__(self, url, response):
        self.url = url
        self.hostname = urllib.request.urlparse(self.url).hostname
        self.soup = LinksSoup(response, "lxml")

    def get_links(self, just_from_root=False, full_path=False, including_tag=False):
        """
        :param just_from_root: bool
            True iff you want only those urls who are from the root path
        :param full_path: bool
            True iff you want to fill relative path to get full path
        :param including_tag: bool
            True iff you want also the tag containing the link
        :return: [] of str
            List of all valid links
        """

        links = self.soup.get_links()  # (tag, link) tuples
        external_links = [
            link for link in links if is_url(link[1])
        ]  # filter just valid urls
        relative_links = [
            link for link in links if is_relative_url(link[1])
        ]

        if just_from_root:
            external_links = [
                link for link in external_links if is_url_from_root(link[1], self.hostname)
            ]

        if full_path:
            relative_links = [
                urllib.parse.urljoin(self.url, link[1]) for link in relative_links
            ]

        links = external_links + relative_links
        if including_tag:
            return [
                link[0] for link in links
            ]  # including tag

        return [
            link[1] for link in links
        ]  # just link, without tag

    def edit_links(self, editor, just_from_root=True):
        """
        :param editor: callable
            Calls this function with each link as parameter
        :param just_from_root: bool
            True iff you want only those urls who are from the root path
        :return: [] of str
            List of all valid links
        """

        links = self.get_links(just_from_root=just_from_root, including_tag=True)
        for link in links:
            editor(link)

    def __str__(self):
        return str(self.soup)
