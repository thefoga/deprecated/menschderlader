# !/usr/bin/python3
# coding: utf-8


""" Data models """

from queue import Queue


class SetQueue(Queue):
    """ A queue which can check if it contains an item """

    def __contains__(self, item):
        with self.mutex:
            return item in self.queue
