# !/usr/bin/python3
# coding: utf_8


""" Command-line interface to menschderlader """

import argparse
import os

from menschderlader.models.bot import Browser
from menschderlader.models.webpage import WebpageParser
from menschderlader.utils.web import get_hostname


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(
        usage="-u <url to download> (-r) -o <output folder>")
    parser.add_argument("-u", dest="url", help="url to download", required=True, type=str)
    parser.add_argument("-r", dest="recurse", action="store_true")
    parser.add_argument("-o", dest="path_out", help="output folder", required=True, type=str)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()
    return str(args.url), bool(args.recurse), str(args.path_out)


def check_args(url, recurse, path_out):
    """
    :param url: str
        Url to download
    :param path_out: str
        Folder to use as output
    :param path_driver: str
        Path to chrome-driver to use
    :return: bool
        True iff args are correct
    """

    assert (url.startswith("http"))
    assert (os.path.isdir(path_out))
    return True


def main():
    url, recurse, path_out = parse_args(create_args())
    if check_args(url, recurse, path_out):
        driver = Browser(url, path_out, verbose=True)
        driver.download(recurse)


def debug():
    u = "http://olodogma.com/wordpress"
    source = open("/home/stefano/Downloads/chrome/olo.html")  # w.get_html_source()
    p = WebpageParser(u, source)

    host_url = get_hostname(u)
    p


if __name__ == '__main__':
    main()
