# !/usr/bin/python3
# coding: utf-8


""" Tools to deal with files and folders """

import ntpath


def get_folder_filename(path):
    """
    :param path: str
        Path to folder or file
    :return: tuple (str or None, str or None)
        Containing folder, file name. Example: "a/b/c.d" => ("a/b", "c.d")
    """

    folder, filename = ntpath.split(path)
    if not folder:
        folder = None

    if not filename:
        filename = None

    return folder, filename


def is_folder(path, path_separator="/"):
    """
    :param path: str
        Path to folder or file
    :param path_separator: str
        Separates path. E.g unix => "/", windows => "\"
    :return: bool
        True iff path is a folder
    """

    if path.endswith(path_separator):
        return True

    path_name = path.split(path_separator)[-1]
    if path_name.startswith("."):
        path_name = path_name[1:]

    return "." not in path_name
