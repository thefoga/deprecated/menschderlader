# !/usr/bin/python3
# coding: utf-8


""" Tools to deal with urls, www and such """

import urllib.request


def is_relative_url(candidate):
    """
    :param candidate: str
        Candidate url
    :return: bool
        True iff candidate is a relative path (url)
    """

    return candidate.startswith("/")


def get_hostname(url):
    """
    :param url: str
        Url
    :return: str
        Name of host of url
    """

    return urllib.request.urlparse(url).hostname


def get_relative_path(url):
    """
    :param url: str
        Url
    :return: str
        Relative (to host) path of url
    """

    result = urllib.request.urlparse(url).path
    if not result.startswith("/"):
        result = "/" + result
    return result


def is_url_from_root(candidate, root_host):
    """
    :param candidate: str
        Candidate url
    :param root_host: str
        Hostname of root url
    :return: bool
        True iff candidate url is from the root host specified
    """

    candidate_host = get_hostname(candidate)
    return candidate_host == root_host
